﻿using System.Data.Entity;
using TellerAutomation.Core.Data;

namespace TellerAutomation.Infrastructure.Data
{
    public class TellerAutomationDbContext : DbContext, ITellerAutomationDbContext
    {
        public TellerAutomationDbContext(string connectionString) : base(connectionString)
        {
        }

        //  protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //  {
        //      optionsBuilder.UseSqlite(@"Data Source=C:\Temp\kudi.db");
        //  }

        //public virtual DbSet<Document> Documents { set; get; }
        //public virtual DbSet<Offer> Offers { get; set; }
    }
}