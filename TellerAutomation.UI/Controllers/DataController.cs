﻿using MediatR;
using System.Threading.Tasks;
using System.Web.Http;
using TellerAutomation.UI.Models;
using TellerAutomation.UI.Services;

namespace TellerAutomation.UI.Controllers
{
    [RoutePrefix("api/data")]
    public class DataController : ApiController
    {
        //private readonly IMediator _mediator;

        public DataController(IMediator mediator)
        {
            //this._mediator = mediator;
        }

        [HttpPost]
        [Route("~/addTransaction")]
        public async Task<Transaction> AddTransaction(string tellerId, string sessionId)
        {
            
            return new Transaction();
        }

        [HttpPost]
        [Route("~/processEntry")]
        public async Task<string> ProcessEntry([FromBody]string data)
        {
            
            return string.Empty;
        }

        [HttpPost]
        [Route("~/getTransaction")]
        public async Task<string> GetTransaction()
        {
            
            return string.Empty;
        }

        [HttpPost]
        [Route("~/approveTransaction")]
        public async Task<string> ApproveTransaction([FromBody]string tellerId, [FromBody]string sessionId)
        {
            
            return string.Empty;
        }

        [HttpPost]
        [Route("~/cancelTransaction")]
        public async Task<string> CancelTransaction([FromBody]string tellerId, [FromBody]string sessionId)
        {
            return string.Empty;
        }

        [HttpPost]
        [Route("~/printReceipt")]
        public async Task<string> PrintReceipt([FromBody]string tellerId, [FromBody]string sessionId)
        {            
            return string.Empty;
        }

        [HttpPost]
        [Route("~/addCBAStatus")]
        public async Task<string> AddCBAStatus([FromBody]string tellerId, [FromBody]string sessionId, 
            [FromBody]bool status, [FromBody]string tranId, [FromBody]string message)
        {
            
            return string.Empty;
        }
    }
}