﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TellerAutomation.UI.Models
{
    public class Transaction
    {
        public string TellerId { set; get; }
        public string SrcAccountNo { set; get; }
        public decimal Amount { set; get; }
        public string SrcAccountName { set; get; }
        public string DstAccountNo { set; get; }
        public string DstAccountName { set; get; }
        
    }
}