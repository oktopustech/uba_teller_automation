﻿using Newtonsoft.Json.Linq;
using System;
using TellerAutomation.Core.Data;

namespace TellerAutomation.UI.Services
{
    public class TrWithdrawal
    {
        private readonly Transaction _transaction;
        private readonly ICBA _cba;
        private const string Step1SelectTranType = "SelectTranType";
        private const string Step2SelectAccountType = "SelectAccountType";
        private const string Step3Amount = "Amount";
        private const string Step4QueryName = "QueryName";
        private const string Step5SendToTeller = "SendToTeller";
        private const string Step6PrintReceipt = "PrintReceipt";
        private const string Step7NewDraft = "NewDraft";

        public TrWithdrawal(Transaction transaction, ICBA cba)
        {
            _transaction = transaction;
            _cba = cba;
        }

        public void Execute()
        {
            var lastUpdate = (string)JObject.Parse(_transaction.LastDeviceUpdate)["data"]["update"]["command"];
            var response = (string)JObject.Parse(_transaction.LastDeviceUpdate)["data"]["update"]["response"];
            /*
             *{
   “data”: {
                  “sessionId”: “0123456789”,
                  “timestamp”: “2017-12-23T18:25:43.511Z”,
                  “update”: {
                                      “command”: “cmdReadInput”,
                                      “response”: “2”
                  }
   } }
            */

            if(_transaction.Status)
            switch (_transaction.LastStep)
            {
                case Step1SelectTranType:
                    ReadAmount(_transaction);
                    break;

                case Step2SelectAccountType:
                    _transaction.TranType = (Core.Reference.TranType)int.Parse(response);
                    ReadAmount(_transaction);
                    break;

                case Step3Amount:
                    _transaction.TranAmount = decimal.Parse(response);
                    //fetch other data
                    string accNo = "";
                    string accName = "";
                    _cba.GetAccountInfo(_transaction.MaskedPAN, _transaction.AccountType, ref accNo, ref accName);
                    _transaction.AccountName = accName;
                    _transaction.AccountNumber = accNo;

                    //send teller
                    _transaction.Status = Core.Reference.Status.Ready;
                    break;
            }

            if (_transaction.AccountType == Core.Reference.AccountType.Unknown)
                ReadAmount(_transaction);

            switch (lastUpdate)
            {
                case Core.Reference.CmdAuthCard:
                    ReadAmount(_transaction);
                    break;

                case Core.Reference.CmdIdle:
                    CmdIdleHandler(_transaction);
                    break;

                case Core.Reference.CmdPrint:
            }
        }

        private void ReadAmount(Transaction transaction)
        {
            JObject o = new JObject(
                    new JProperty("data",
                    new JObject(
                        new JProperty("sessionId", _transaction.SessionId),
                        new JProperty("timestamp", DateTime.Now),
                        new JProperty("command",
                            new JObject(
                            new JProperty("name", Core.Reference.CmdReadInput),
                            new JProperty("message",
                                new JObject(
                                    new JProperty("title", "Select account"),
                                    new JProperty("details",
                                        new JArray(
                                            new JValue(string.Format("{0} - {1}", (int)Core.Reference.AccountType.Current, Core.Reference.AccountType.Current)),
                                            new JValue(string.Format("{0} - {1}", (int)Core.Reference.AccountType.Saving, Core.Reference.AccountType.Saving)),
                                            new JValue(string.Format("{0} - {1}", (int)Core.Reference.AccountType.Credit, Core.Reference.AccountType.Credit))
                                        )
                                )
                            )
                        ))),
                  new JProperty("response",
                        new JObject(
                            new JProperty("size", new JValue(1))
                        )
                    ))));

            transaction.DeviceCmd = o.ToString();
        }

        private void CmdAuthCardHandler(Transaction transaction)
        {
        }

        public string GetJsonData()
        {
            JObject o = new JObject(
                new JProperty("data",
                    new JObject(
                        new JProperty("sessionId", _transaction.SessionId),
                        new JProperty("timestamp", DateTime.Now),
                        new JProperty("command",
                            new JObject(
                                new JProperty("name", Core.Reference.CmdAuthCard),
                                new JProperty("message",
                                    new JObject(
                                       new JProperty("title", "Enter your PIN")
                                    )
                                )
                            )
                        ),
                       new JProperty(
                           "pan", _transaction.MaskedPAN
                       )
                    )
               )
            );

            return o.ToString();
        }
    }
}