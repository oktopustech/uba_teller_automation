﻿using Newtonsoft.Json.Linq;
using System;
using TellerAutomation.Core.Data;

namespace TellerAutomation.UI.Services
{
    public class DeviceCommandAuthCard
    {
        private Transaction _transaction;

        public DeviceCommandAuthCard(Transaction transaction)
        {
            _transaction = transaction;
        }

        public string GetJsonData()
        {
            JObject o = new JObject(
                new JProperty("data",
                    new JObject(
                        new JProperty("sessionId", _transaction.SessionId),
                        new JProperty("timestamp", DateTime.Now),
                        new JProperty("command",
                            new JObject(
                                new JProperty("name", Core.Reference.CmdAuthCard),
                                new JProperty("message",
                                    new JObject(
                                       new JProperty("title", "Enter your PIN")
                                    )
                                )
                            )
                        ),
                       new JProperty(
                           "pan", _transaction.MaskedPAN
                       )
                    )
               )
            );

            return o.ToString();
        }
    }
}