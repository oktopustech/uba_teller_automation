﻿using Newtonsoft.Json.Linq;
using System;
using TellerAutomation.Core.Data;

namespace TellerAutomation.UI.Services
{
    public class DeviceCommandReadPAN
    {
        private Transaction _transaction;

        public DeviceCommandReadPAN(Transaction transaction)
        {
            _transaction = transaction;
        }

        public string GetJsonData()
        {
            JObject o = new JObject(
                new JProperty("data",
                    new JObject(
                        new JProperty("sessionId", _transaction.SessionId),
                        new JProperty("timestamp", DateTime.Now),
                        new JProperty("command",
                            new JObject(
                                new JProperty("name", Core.Reference.CmdReadPAN),
                                new JProperty("message",
                                    new JObject(
                                       new JProperty("title", "Insert your card...")
                                    )
                                )
                            )
                        )
                    )
               )
            );

            return o.ToString();
        }
    }
}