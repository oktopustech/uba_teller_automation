﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using TellerAutomation.Core.Data;

namespace TellerAutomation.UI.Services
{
    public class DeviceCommandIdle
    {
        private Transaction _transaction;

        public DeviceCommandIdle(Transaction transaction)
        {
            _transaction = transaction;
        }

        public string GetJsonData()
        {
            //string.Format("[[[welcome %1, today is %0|||{0}|||{1}]]]", day, name)

            JObject o = new JObject(
                new JProperty("data",
                    new JObject(
                        new JProperty("sessionId", _transaction.SessionId),
                        new JProperty("timestamp", DateTime.Now),
                        new JProperty("command",
                            new JObject(
                                new JProperty("name", Core.Reference.CmdIdle),
                                new JProperty("message",
                                    new JObject(
                                       new JProperty("title", "Please wait...")
                                    )
                                )
                            )
                        ),
                        new JProperty("response",
                            new JObject("idleInterval", ConfigurationManager.AppSettings["deviceIdleTime"])
                        )
                    )
               )
            );

            return o.ToString();
        }
    }
}