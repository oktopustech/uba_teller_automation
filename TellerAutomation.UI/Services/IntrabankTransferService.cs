﻿using Newtonsoft.Json.Linq;
using System;
using TellerAutomation.Core.Data;

namespace TellerAutomation.UI.Services
{
    internal class IntrabankTransferService
    {
        private readonly IRepository _repository;
        private readonly ICardMgmt _cardMgmt;
        private readonly ICBA _cBA;

        public IntrabankTransferService(IRepository repository, ICardMgmt cardMgmt, ICBA cBA)
        {
            _repository = repository;
            _cardMgmt = cardMgmt;
            _cBA = cBA;
        }

        public bool ProcessDevice(string update, Transaction txn)
        {
            //are we suppose to be here?
            if (txn.TranType != Core.Reference.TranType.IntrabankTransfer)
                return true;

            var cmd = (string)JObject.Parse(update)["update"]["command"];
            var response = (string)JObject.Parse(update)["update"]["response"];

            //read amount
            if (!txn.TranAmount.HasValue)
            {
                if (!decimal.TryParse(response, out decimal amt))
                {
                    txn.DeviceCmd = GenerateReadAmount(txn);
                    _repository.Cache(txn);
                    return false;
                }

                txn.TranAmount = amt;
                _repository.Cache(txn);
                response = "";
            }

            //read beneficiary
            if (string.IsNullOrEmpty(txn.BenfAcctNumber))
            {
                if (string.IsNullOrEmpty(response))
                {
                    txn.DeviceCmd = GenerateReadBenfAccount(txn);
                    _repository.Cache(txn);
                    return false;
                }

                txn.BenfAcctNumber = response;
                _repository.Cache(txn);
                response = "";                
            }

            //confirm name
            if (!string.IsNullOrEmpty(txn.BenfAcctNumber) && string.IsNullOrEmpty(txn.BenfName))
            {
                var accName = "";
                var isCustomer = false;
                _cBA.GetAccountInfo(txn.AccountNumber, ref accName, ref isCustomer);
                txn.BenfName = accName;

                txn.DeviceCmd = GenerateReceipt(txn);
                _repository.Cache(txn);
                return false;
            }

            //2 cancel
            if (response.Equals("2"))
            {
                txn.Status = Core.Reference.Status.CANCELLED;
                _repository.Cache(txn);

                txn.DeviceCmd = new DeviceIdleService(_repository).GetMessage(txn, "Remove your card");
                return false;
            }

            if (response.Equals("1"))
            {
                txn.Status = Core.Reference.Status.TELLER_ACTION;
            }

            //1 continue
            if (txn.Status == Core.Reference.Status.TELLER_ACTION)
            {
                txn.DeviceCmd = new DeviceIdleService(_repository).GetMessage(txn, "Please wait...");
                _repository.Cache(txn);
                return false;
            }

            if(txn.Status == Core.Reference.Status.SUCCESS)
            {
                txn.DeviceCmd = GenerateReceipt(txn);
                _repository.Cache(txn);

                return false;
            }

            if(txn.IsCustReceiptPrinted ==  )

            if (!string.IsNullOrEmpty(cmd) && cmd.Equals(Core.Reference.CmdReadInput) && !txn.TranAmount.HasValue)
            {
                txn.TranAmount = (decimal)JObject.Parse(update)["update"]["response"];
                //fetch the rest data
                var acctNo = "";
                var acctName = "";
                var acctCtry = "";
                if (!_cardMgmt.GetAccountInfo(txn.PAN, txn.AccountType, ref acctName, ref acctNo, ref acctCtry))
                {
                    txn.Status = Core.Reference.Status.TELLER_ACTION;
                    txn.Message = string.Format("{0} Account not found", txn.AccountType);
                    txn.DeviceCmd = new DeviceIdleService(_repository).GetMessage(txn, "Please wait...");
                    return false;
                }

                txn.AccountNumber = acctNo;
                txn.AccountName = acctName;
                txn.AccountCountry = acctCtry;
                txn.Status = Core.Reference.Status.TELLER_ACTION;
                _repository.Cache(txn);

                return false;
            }


            //pass to teller
            if (txn.Status == Core.Reference.Status.TELLER_ACTION)
            {
                txn.DeviceCmd = new DeviceIdleService(_repository).GetMessage(txn, update);
                _repository.Cache(txn);
                return false;
            }

            if (!txn.TranAmount.HasValue)
            {
                txn.DeviceCmd = GenerateReadAmount(txn);
                txn.LastDeviceUpdate = update;
                txn.Status = Core.Reference.Status.TRAN_ENTRIES;
                _repository.Cache(txn);
            }

            return false;
        }

        private string GenerateReceipt(Transaction txn)
        {
            Object o = new JObject(
                    new JProperty("tellerId", txn.TellerId),
                    new JProperty("deviceId", txn.DeviceId),
                    new JProperty("sessionId", txn.SessionId),
                    new JProperty("timestamp", DateTime.Now),
                    new JProperty("command",
                    new JObject(
                            new JProperty("name", Core.Reference.CmdReadInput),
                            new JProperty("message",
                                new JObject(
                                    new JProperty("title", txn.BenfName),
                                    new JProperty("details",
                                        new JArray(
                                            "1 - Continue",
                                            "2 - Cancel"
                                            )
                                    )
                        )))
                    ));

            return o.ToString();
        }

        public string GenerateReadAmount(Transaction txn)
        {
            JObject o = new JObject(
                    new JProperty("tellerId", txn.TellerId),
                    new JProperty("deviceId", txn.DeviceId),
                    new JProperty("sessionId", txn.SessionId),
                    new JProperty("timestamp", DateTime.Now),
                    new JProperty("command",
                    new JObject(
                            new JProperty("name", Core.Reference.CmdReadInput),
                            new JProperty("message",
                                new JObject(
                                    new JProperty("title", "Enter amount")
                        )))
                    ));

            return o.ToString();
        }

        public string GenerateReadBenfAccount(Transaction txn)
        {
            JObject o = new JObject(
                    new JProperty("tellerId", txn.TellerId),
                    new JProperty("deviceId", txn.DeviceId),
                    new JProperty("sessionId", txn.SessionId),
                    new JProperty("timestamp", DateTime.Now),
                    new JProperty("command",
                    new JObject(
                            new JProperty("name", Core.Reference.CmdReadInput),
                            new JProperty("message",
                                new JObject(
                                    new JProperty("title", "Enter beneficiary account")
                        )))
                    ));

            return o.ToString();
        }
    }
}