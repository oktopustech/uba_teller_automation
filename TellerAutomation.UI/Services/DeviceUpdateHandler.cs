﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using TellerAutomation.Core.Data;

namespace TellerAutomation.UI.Services
{
    public class DeviceUpdateHandler : IRequestHandler<DeviceUpdate, DeviceCommand>
    {
        public string JsonData { get; set; }
        public string Address { set; get; }

        private readonly IRepository _repo;

        public DeviceUpdateHandler(IRepository repo)
        {
            _repo = repo;
        }

        public Task<DeviceCommand> Handle(DeviceUpdate request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}