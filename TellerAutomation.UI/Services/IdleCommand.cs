﻿using System;
using Microsoft.AspNet.SignalR;
using TellerAutomation.Core.Data;

namespace TellerAutomation.UI.Services
{
    public class IdleCommand : Hub
    {
        private IRepository _repository;

        public IdleCommand(IRepository repository)
        {
            _repository = repository;
        }

        public void Update(string tellerId, string message)
        {
            Clients.All.Command(tellerId + " " + message);
        }

        internal string Generate(Transaction txn)
        {
            throw new NotImplementedException();
        }
    }
}