﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using TellerAutomation.Core.Data;

namespace TellerAutomation.UI.Services
{
    public class DeviceIdleService
    {
        private static readonly log4net.ILog log
            = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //private static readonly ILog log = LogManager.GetLogger(typeof(RegisterDevice));

        private readonly IRepository _repo;

        public DeviceIdleService(IRepository repo)
        {
            _repo = repo;
        }

        public string GetMessage(Transaction txn, string message)
        {            
            var o = new JObject(
                    new JProperty("tellerId", txn.TellerId),
                    new JProperty("deviceId", txn.DeviceId),
                    new JProperty("sessionId", txn.SessionId),
                    new JProperty("timestamp", DateTime.Now),
                    new JProperty("command", 
                        new JObject(
                            new JProperty("name", Core.Reference.CmdIdle),
                            new JProperty("message", 
                                new JObject(
                                    new JProperty("title", message
                                )
                            ),
                            new JProperty("idleInterval", ConfigurationManager.AppSettings["deviceIdleTime"])
                       )
                    )
                );

            return o.ToString();
        }

      }
}