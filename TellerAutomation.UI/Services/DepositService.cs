﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TellerAutomation.Core.Data;

namespace TellerAutomation.UI.Services
{
    public class DepositService
    {
        private readonly IRepository _repository;
        private readonly ICardMgmt _cardMgmt;

        public DepositService(IRepository repository, ICardMgmt cardMgmt)
        {
            _repository = repository;
            _cardMgmt = cardMgmt;
        }

        public bool ProcessDevice(string update, Transaction txn)
        {              
            //are we suppose to be here?
            if (txn.TranType != Core.Reference.TranType.Deposit)
                return true;

            var cmd = (string)JObject.Parse(update)["update"]["command"];

            //read amount
            if (!string.IsNullOrEmpty(cmd) && cmd.Equals(Core.Reference.CmdReadInput) && !txn.TranAmount.HasValue)
            {
                txn.TranAmount = (decimal)JObject.Parse(update)["update"]["response"];
                //fetch the rest data
                var acctNo = "";
                var acctName = "";
                var acctCtry = "";
                if(!_cardMgmt.GetAccountInfo(txn.PAN, txn.AccountType, ref acctName, ref acctNo, ref acctCtry))
                {
                    txn.Status = Core.Reference.Status.TELLER_ACTION;
                    txn.Message = string.Format("{0} Account not found", txn.AccountType);
                    txn.DeviceCmd = new DeviceIdleService(_repository).GetMessage(txn, "Please wait...");
                    return false;
                }

                txn.AccountNumber = acctNo;
                txn.AccountName = acctName;
                txn.AccountCountry = acctCtry;
                txn.Status = Core.Reference.Status.TELLER_ACTION;
                _repository.Cache(txn);

                return false;
            }
            
            
            //pass to teller
            if(txn.Status == Core.Reference.Status.TELLER_ACTION)
            {
                txn.DeviceCmd = new DeviceIdleService(_repository).GetMessage(txn, update);
                _repository.Cache(txn);
                return false;
            }

            if (!txn.TranAmount.HasValue)
            {                
                txn.DeviceCmd = GenerateReadAmount(txn);
                txn.LastDeviceUpdate = update;
                txn.Status = Core.Reference.Status.TRAN_ENTRIES;
                _repository.Cache(txn);
            }

            return false;            
        }

        public string GenerateReadAmount(Transaction txn)
        {            
            JObject o = new JObject(
                    new JProperty("tellerId", txn.TellerId),
                    new JProperty("deviceId", txn.DeviceId),
                    new JProperty("sessionId", txn.SessionId),
                    new JProperty("timestamp", DateTime.Now),
                    new JProperty("command",
                    new JObject(
                            new JProperty("name", Core.Reference.CmdReadInput),
                            new JProperty("message",
                                new JObject(
                                    new JProperty("title", "Enter amount")
                        )))
                    ));

            return o.ToString();
        }
    }
}