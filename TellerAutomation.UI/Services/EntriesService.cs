﻿using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TellerAutomation.Core.Data;

namespace TellerAutomation.UI.Services
{
    public class EntriesService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(EntriesService));
        private readonly IRepository _repository;
        
        
        /*
{
    “tellerId”:”tn019838”,
    “deviceId”:”man-prod-serial”,
    “sessionId”: “0123456789”, 
    “timestamp”: “2017-12-23T18:25:43.511Z”, 
    “update”: {
            “command”: “cmdIdle”,
            “response”: “173a::93n1::918”
      }
}

*/

       

        public EntriesService(Core.Data.IRepository repository)
        {
            _repository = repository;
        }

        public string Process(string entry, string ipAddr)
        {
            if (string.IsNullOrEmpty(entry))
            {
                _log.InfoFormat("Invalid update from addr {0}", ipAddr);
                return @"{error: 'Invalid entry'}";
            }

            var tellerId = (string)JObject.Parse(entry)["tellerId"];
            var sessionId = (string)JObject.Parse(entry)["sessionId"];
            
            var txn = _repository.GetLastTransaction(tellerId);
            if (txn == null || string.IsNullOrEmpty(txn.SessionId) || string.IsNullOrEmpty(sessionId) || !txn.SessionId.Equals(sessionId))
            {
                txn = new Transaction { SessionId = Guid.NewGuid().ToString(), TellerId = tellerId };
                txn.Status = Core.Reference.Status.READ_PAN;
                txn.DeviceId = (string)JObject.Parse(entry)["deviceId"];
            }

            if (ProcessReadPAN(entry, txn) == false)
                return txn.DeviceCmd;

            if (ProcessAuthCard(entry, txn) == false)
                return txn.DeviceCmd;

            if (ProcessAccount(entry, txn) == false)
                return txn.DeviceCmd;

            if (ProcessTranType(entry, txn) == false)
                return txn.DeviceCmd;

            if (new DepositService(_repository).ProcessDevice(entry, txn) == false)
                return txn.DeviceCmd;

            if (new WithdrawalService(_repository).ProcessDevice(entry, txn) == false)
                return txn.DeviceCmd;

            if (new IntrabankTransferService(_repository).ProcessDevice(entry, txn) == false)
                return txn.DeviceCmd;

            if (new InterbankTransferService(_repository).ProcessDevice(entry, txn) == false)
                return txn.DeviceCmd;

            return new IdleCommand(_repository).Generate(txn);
        }

        public bool ProcessTranType(string update, Transaction txn)
        {
            //check tran type is in update
            var cmd = (string)JObject.Parse(update)["update"]["command"];
            if (!string.IsNullOrEmpty(cmd) && cmd.Equals(Core.Reference.CmdReadInput)
                && txn.Status == Core.Reference.Status.SELECT_TRAN_TYPE)
            {
                txn.TranType = (Core.Reference.TranType)(int)JObject.Parse(update)["update"]["response"];
            }

            //confirm tran okay
            if (txn.TranType != Core.Reference.TranType.Unknown)
            {
                txn.LastDeviceUpdate = update;
                txn.Status = Core.Reference.Status.TRAN_ENTRIES;
                _repository.Cache(txn);
                return true;
            }

            //or request for device to tran type
            JObject o = new JObject(
                    new JProperty("tellerId", txn.TellerId),
                    new JProperty("deviceId", txn.DeviceId),
                    new JProperty("sessionId", txn.SessionId),
                    new JProperty("timestamp", DateTime.Now),
                    new JProperty("command",
                    new JObject(
                            new JProperty("name", Core.Reference.CmdReadInput),
                            new JProperty("message",
                                new JObject(
                                    new JProperty("details", new JArray(
                                            string.Format("{0} - Cash Deposit",(int)Core.Reference.TranType.Deposit),
                                            string.Format("{0} - Cash Withdrawal", (int)Core.Reference.TranType.Withdrawal),
                                            string.Format("{0} - Transfer (UBA)", (int)Core.Reference.TranType.IntrabankTransfer),
                                            string.Format("{0} - Transfer (Otherbank)", (int)Core.Reference.TranType.InterbankTransfer)
                                        )
                            )
                        )))
                    ));
            txn.DeviceCmd = o.ToString();
            txn.LastDeviceUpdate = update;
            txn.Status = Core.Reference.Status.SELECT_TRAN_TYPE;
            _repository.Cache(txn);

            return false;
        }

        public bool ProcessAccount(string update, Transaction txn)
        {
            //check account selection is in update
            var cmd = (string)JObject.Parse(update)["update"]["command"];
            if (!string.IsNullOrEmpty(cmd) && cmd.Equals(Core.Reference.CmdReadInput) 
                && txn.Status == Core.Reference.Status.SELECT_ACCOUNT)
            {
                txn.AccountType = (Core.Reference.AccountType) (int)JObject.Parse(update)["update"]["response"];
            }

            //confirm auth okay
            if (txn.AccountType != Core.Reference.AccountType.Unknown)
            {
                txn.LastDeviceUpdate = update;
                _repository.Cache(txn);
                return true;
            }

            //or request for device to auth pan
            JObject o = new JObject(
                    new JProperty("tellerId", txn.TellerId),
                    new JProperty("deviceId", txn.DeviceId),
                    new JProperty("sessionId", txn.SessionId),
                    new JProperty("timestamp", DateTime.Now),
                    new JProperty("command",
                    new JObject(
                            new JProperty("name", Core.Reference.CmdReadInput),
                            new JProperty("message",
                                new JObject(
                                    new JProperty("title", "Select account"),
                                    new JProperty("details", new JArray(
                                            "1 - Current",
                                            "2 - Savings",
                                            "3 - Credit"
                                        )
                            )
                        )))
                    ));
            txn.DeviceCmd = o.ToString();
            txn.LastDeviceUpdate = update;
            txn.Status = Core.Reference.Status.SELECT_ACCOUNT;
            _repository.Cache(txn);


            return false;
        }

        public bool ProcessAuthCard(string update, Transaction txn)
        {
            //check auth is in update
            var cmd = (string)JObject.Parse(update)["update"]["command"];
            if (!string.IsNullOrEmpty(cmd) && cmd.Equals(Core.Reference.CmdAuthCard))
            {
                txn.IsCardVerified = (bool)JObject.Parse(update)["update"]["response"];
            }

            //confirm auth okay
            if (txn.IsCardVerified)
            {
                txn.LastDeviceUpdate = update;
                _repository.Cache(txn);
                return true;
            }

            //or request for device to auth pan
            JObject o = new JObject(
                    new JProperty("tellerId", txn.TellerId),
                    new JProperty("deviceId", txn.DeviceId),
                    new JProperty("sessionId", txn.SessionId),
                    new JProperty("timestamp", DateTime.Now),
                    new JProperty("command",
                    new JObject(
                            new JProperty("name", Core.Reference.CmdAuthCard),
                            new JProperty("message",
                                new JObject(
                                    new JProperty("title", "Enter your PIN")
                            ),
                            new JProperty("pan", txn.MaskedPAN)
                        )))
                    );
            txn.DeviceCmd = o.ToString();
            txn.LastDeviceUpdate = update;
            txn.Status = Core.Reference.Status.AUTH_CARD;
            _repository.Cache(txn);


            return false;
        }

        public bool ProcessReadPAN(string update, Transaction txn)
        {
            //check pan is in update
            var cmd = (string)JObject.Parse(update)["update"]["command"];
            if(!string.IsNullOrEmpty(cmd) && cmd.Equals(Core.Reference.CmdReadPAN))
            {
                txn.PAN = (string)JObject.Parse(update)["update"]["response"];
                txn.MaskedPAN = MaskPAN(txn.PAN);                
            }

            //confirm pan okay
            if (!string.IsNullOrEmpty(txn.MaskedPAN) && txn.MaskedPAN.Contains("***"))
            {
                txn.LastDeviceUpdate = update;                
                _repository.Cache(txn);
                return true;
            }

            //or request for device to read pan
            JObject o = new JObject(
                    new JProperty("tellerId", txn.TellerId),
                    new JProperty("deviceId", txn.DeviceId),
                    new JProperty("sessionId", txn.SessionId),
                    new JProperty("timestamp", DateTime.Now),
                    new JProperty("command",
                    new JObject(
                            new JProperty("name", Core.Reference.CmdReadPAN),
                            new JProperty("message",
                                new JObject(
                                    new JProperty("title", "Insert your card…")
                            )
                        )))
                    );
                txn.DeviceCmd = o.ToString();
                txn.LastDeviceUpdate = update;
                txn.Status = Core.Reference.Status.READ_PAN;
                _repository.Cache(txn);


            return false;
        }

        public string MaskPAN(string pan)
        {
            if (string.IsNullOrEmpty(pan) || pan.Length < 7)
                return string.Empty;

            var masked = pan.Substring(0, 6) + "******" + pan.Substring(pan.Length-4);
            return masked;
        }
        
    }



     
}