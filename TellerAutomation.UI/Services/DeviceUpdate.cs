﻿using MediatR;

namespace TellerAutomation.UI.Services
{
    public class DeviceUpdate : IRequest<DeviceCommand>
    {
        public string JsonData { get; set; }
        public string Address { set; get; }
    }
}