﻿namespace TellerAutomation.Core
{
    public static class Reference
    {
        public static string CmdRegisterDevice = "cmdRegisterDevice";
        public const string CmdReadPAN = "cmdReadPAN";
        public const string CmdAuthCard = "cmdAuthCard";
        public const string CmdReadInput = "cmdReadInput";
        public const string CmdIdle = "cmdIdle";
        public const string CmdPrint = "cmdPrint";

        public const string ActionDeviceReadPAN = "ReadPAN";
        public const string ActionDeviceAuthCard = "AuthCard";
        public const string ActionDeviceSelectAccountType = "SelectAccountType";
        public const string ActionDeviceSelectTranType = "SelectTranType";
        public const string ActionDeviceWithdrawalEnterAmount = "WithdrawalEnterAmount";
        public const string ActionDeviceWithdrawalEnterAmount = "";

        public enum AccountType
        {
            Unknown = 0,
            Current = 1,
            Saving = 2,
            Credit = 3,
            Office = 4
        }
        

        public enum TranType
        {
            Unknown = 0,
            Deposit = 1,
            Withdrawal = 2,
            IntrabankTransfer = 3,
            InterbankTransfer = 4
        }

        /*
    private static readonly string STEP1_READ_CARD = "read card";
        private static readonly string STEP2_AUTH_CARD = "auth card";
        private static readonly string STEP3_SELECT_ACCOUNT = "select account";
        private static readonly string STEP4_SELECT_TRAN = "select tran";
        private static readonly string STEP5_READ_TRAN = "read tran";
    */

        public enum Status
        {
            READ_PAN = 0,
            AUTH_CARD = 2,
            SELECT_ACCOUNT = 3,
            SELECT_TRAN_TYPE = 4,
            TRAN_ENTRIES = 5,
            TELLER_ACTION = 6,
            PROCESSING = 7,
            ERROR = 8,
            CANCELLED = 9,
            SUCCESS = 1
        }
    }
}