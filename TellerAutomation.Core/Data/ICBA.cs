﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TellerAutomation.Core.Data
{
    public interface ICBA
    {
        //void GetAccountInfo(string pan, Core.Reference.AccountType accType, ref string acctNo, ref string name);
        void GetAccountInfo(string acctNo, ref string name, ref bool isCustomer);
    }
}
