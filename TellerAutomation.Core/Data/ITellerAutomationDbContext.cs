﻿using System;

namespace TellerAutomation.Core.Data
{
    public interface ITellerAutomationDbContext : IDisposable
    {
        int SaveChanges();
    }
}