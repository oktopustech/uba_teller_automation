﻿using System;

namespace TellerAutomation.Core.Data
{
    public interface IRepository
    {
        Transaction GetLastTransaction(string tellerId);

        Transaction Cache(Transaction transaction);

        Transaction Persist(Transaction transaction);
    }

    
}