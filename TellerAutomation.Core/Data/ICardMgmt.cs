﻿namespace TellerAutomation.Core.Data
{
    public interface ICardMgmt
    {
        bool GetAccountInfo(string pan, Reference.AccountType accountType, ref string name, ref string number, ref string country);
        
    }
}