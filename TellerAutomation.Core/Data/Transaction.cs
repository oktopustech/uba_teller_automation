﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TellerAutomation.Core.Data
{
    /*
    private static readonly string STEP1_READ_CARD = "read card";
        private static readonly string STEP2_AUTH_CARD = "auth card";
        private static readonly string STEP3_SELECT_ACCOUNT = "select account";
        private static readonly string STEP4_SELECT_TRAN = "select tran";
        private static readonly string STEP5_READ_TRAN = "read tran";
    */
    public class Transaction
    {
        public Transaction()
        {
            this.AccountType = Reference.AccountType.Unknown;
            this.TranType = Reference.TranType.Unknown;
        }

        public Core.Reference.TranType TranType { set; get; }
        public Core.Reference.AccountType AccountType { set; get; }
        public string AccountNumber { set; get; }
        public string AccountName { set; get; }
        public string AccountCountry { set; get; }
        public decimal? TranAmount { set; get; }
        public string Currency { set; get; }
        public string RefNumber { set; get; }
        public string TranID { set; get; }
        public Core.Reference.Status Status { set; get; }
        public bool IsCardVerified { set; get; }
        public string Message { set; get; }
        public string BenfAcctNumber { set; get; }
        public string BenfName { set; get; }
        public string BenfCountry { set; get; }
        public string BenfBankCode { set; get; }
        public string BenfBankName { set; get; }
        public string TellerId { set; get; }
        public string DeviceId { set; get; }
        public string SessionId { set; get; }
        public DateTime EntryTime { set; get; }
        public DateTime LastModified { set; get; }
        public string DeviceModel { set; get; }
        public string LastDeviceUpdate { set; get; }
        public string DeviceCmd { set; get; }
        public string MaskedPAN { set; get; }
        public string LastStep { set; get; }
        public bool IsCardInserted { get; set; }
        public string PAN { get; set; }
        public bool? IsConfirmed { set; get; }
        public bool? IsCustReceiptPrinted { set; get; }
    }
}
